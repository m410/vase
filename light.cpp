#include "light.h"
#include <Arduino.h>
#include "hardware/pio.h"

#define MIN_DUTY 0
#define MAX_DUTY 255
#define RANDOM_SEED_DELAY 8
#define RANDOM_SEED_SPEED_MIN 1
#define RANDOM_SEED_SPEED_MAX 20


/*
Working exccept fo pause at end and restart of light.
*/

extern bool DEBUG; 


LightState* _createLightState(long timestamp, int delay, int speed) {
  if(DEBUG) {
    Serial.print("[_createLightState] timestamp=");
    Serial.print(timestamp);
    Serial.print(", delay=");
    Serial.print(delay);
    Serial.print(", speed=");
    Serial.println(speed);
  }


  LightState* state = new LightState;
  state->timestamp = timestamp;
  state->brightness = 0;
  state->grow = true;
  state->delay = delay;
  state->speed = speed;

  return state;
}

void _deleteLightState(LightState* state) {
  if (DEBUG) {
    Serial.println("[_deleteLightState]");
  }
  delete state;
}

Light* _createLight(int pin, long timestamp, int delaySeed, int speed) {
  if (DEBUG) {
    Serial.print("[_createLight] pin=");
    Serial.print(pin);
    Serial.print(", timestamp=");
    Serial.print(timestamp);
    Serial.print(", delaySeed=");
    Serial.print(delaySeed);
    Serial.print(", speed=");
    Serial.println(speed);
  }

  Light* light = new Light;
  light->pin = pin;
  light->lightState = nullptr;
  return light;
}

void _deleteLight(Light* light) {
  if(DEBUG) {
    Serial.println("[_deleteLight]");
  }
  delete light;
}

void _initState(Light* light, long timestamp, int delaySeed, int speed) {
  if(DEBUG) {
    Serial.print("[_initState] timestamp=");
    Serial.print(timestamp);
    Serial.print(", delay=");
    Serial.print(delaySeed);
    Serial.print(", speed=");
    Serial.println(speed);
  }

  int delay = 1000 * delaySeed;
  light->lightState = _createLightState(timestamp, delay, speed);
}

LightState* _getState(Light* light) {
  if(DEBUG) {
    Serial.println("[_getState] ");
  }
  return light->lightState;
}

void _setState(Light* light, LightState* state) {
  if( DEBUG) {
    Serial.println("[_setState] ");
  }
  light->lightState = state;
}


void _deleteLights(Light** lights, int lightArraySize) {
  if (DEBUG) {
    Serial.println("[_deleteLights] ");
  }

  for (int i = 0; i < lightArraySize; i++) {
    _deleteLight(lights[i]);
  }
  delete[] lights;
}



void stop(Light** lightArrayPtr, int lightArraySize) {
   if(DEBUG){
     Serial.println("[stop]");
   }

  for (int i = 0; i < lightArraySize; i++) {
    Light* light = lightArrayPtr[i];
    
    if(light->lightState != nullptr) {
      digitalWrite(light->pin, LOW);
      _deleteLightState(light->lightState);
      _setState(light, nullptr);
    }
  }
}

void run(Light** lightArrayPtr, int lightArraySize) {
  if( DEBUG) {
    Serial.println("[run]");
  }

  long currentTimestamp = millis();

  for (int i = 0; i < lightArraySize; i++) {
    Light* light = lightArrayPtr[i];
    LightState* state = _getState(light);

    if (state == nullptr) {
      int delay = random(RANDOM_SEED_DELAY);
      int speed = random(RANDOM_SEED_SPEED_MIN, RANDOM_SEED_SPEED_MAX);
      _initState(light, millis(), delay, speed);
      analogWrite(light->pin, 0); // start off
    }

    if (DEBUG) {
      Serial.print("[run] pin=");
      Serial.print(light->pin);
      Serial.print(", grow=");
      Serial.print(state->grow);
      Serial.print(", timestamp=");
      Serial.print(state->timestamp);
      Serial.print(", speed=");
      Serial.print(state->speed);
      Serial.print(", bright=");
      Serial.println(state->brightness);
    }


    if (state->delay <= (currentTimestamp - state->timestamp)) {
      if (state->brightness >= MAX_DUTY) {
        state->grow = false;
        state->brightness = MAX_DUTY;
      } else if (state->brightness <= MIN_DUTY) {
        state->grow = true;
        state->brightness = MIN_DUTY;
      }

      if (state->brightness <= MAX_DUTY && state->grow) {
        state->brightness += state->speed;
      } else if (state->brightness >= MIN_DUTY && !state->grow) {
        if(state->speed < state->brightness) {
          state->brightness -= state->speed;
        } else {
          // make it restart with new delay
          _deleteLightState(state);
          light->lightState = nullptr;
        }
      }

      if (state != nullptr && state->brightness >= MIN_DUTY && state->brightness <= MAX_DUTY) {
        analogWrite(light->pin, state->brightness);
      }

    } else {
      if( DEBUG) {
        Serial.println("[run] delay...");
      }

      analogWrite(light->pin, 0); // start off
    }
  }
}


Light** createLightArray(const int* pinsArray, int pinsSize) {
  if(DEBUG) {
    Serial.println("[createLightArray] ");
  }
  Light** lights = new Light*[pinsSize];
  
  for (int i = 0; i < pinsSize; i++) {
    lights[i] = _createLight(pinsArray[i], 0, 0, 0);
  }
  return lights;
}

void setLightState(bool onState, Light** lightArrayPtr, int lightArraySize) {
  if(DEBUG) {
    Serial.print("[setLightState] ");
    Serial.println(onState);
  }

  if(onState) {
    run(lightArrayPtr, lightArraySize);
  } else {
    stop(lightArrayPtr, lightArraySize);
  }
}

void initLightArray(const int* pinsArray, int pinsSize) {
  for (int i = 0; i < pinsSize; i++) {
    pinMode(pinsArray[i], OUTPUT);
  }
}

void blinkStart() {
  pinMode(LED_BUILTIN, OUTPUT);
  
  // blink at staratup
  digitalWrite(LED_BUILTIN, HIGH); 
  delay(200);   
  digitalWrite(LED_BUILTIN, LOW); 
  delay(200);   
  digitalWrite(LED_BUILTIN, HIGH); 
  delay(200);   
  digitalWrite(LED_BUILTIN, LOW); 
}
