
#ifndef LIGHT_H_
#define LIGHT_H_


struct LightState {
  unsigned long timestamp;
  unsigned int delay;
  unsigned int speed;
  unsigned int brightness;
  bool grow;
};

struct Light {
  int pin;
  LightState* lightState;
};

void initLightArray(const int* pinsArray, int pinsSize);

Light** createLightArray(const int* pinsArray, int pinsSize);

void setLightState(bool onState, Light** lightArrayPtr, int lightArraySize);

void blinkStart();

#endif  // LIGHT_H_